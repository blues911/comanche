<?php

/* app settings */

return [
    'db' => [
        'dsn' => 'mysql',
        'hostname' => '',
        'username' => '',
        'password' => '',
        'database' => '',
        'charset' => 'utf8'
    ],
    'url' => [
        '' => 'test/index',
        'test' => 'test/index'
    ]
];