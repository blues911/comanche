<?php

namespace app\controllers;

use src\mod\Controller;
use app\models\Test;

class TestController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->tpl->layout = 'test';
    }

    public function indexAction()
    {
        $model = new Test();

        $this->tpl->renderFile('index', [
            'var' => $model->var
        ]);
    }
}
