<?php

// dev mode, supports yes/no params
define('DEVELOPMENT', 'yes');

// main folders
define('APP_DIR', __DIR__ . '/app/');
define('SRC_DIR', __DIR__ . '/src/');

// handle dev mode
switch (DEVELOPMENT) {
    case 'yes':
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        break;
    case 'no':
        error_reporting(0);
        break;
    default:
        header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
        echo 'DEVELOPMENT mode is not set correctly.';
        exit(1);
}

include_once SRC_DIR . 'init/kernel.php';
include_once SRC_DIR . 'helpers.php';

// init router
use src\mod\Router;

$router = new Router();
$router->direct();