<?php

namespace src\mod;

class Router
{
    /**
     * Contains current route
     * @var string
     */
    private $route;

    /**
     * Returns requested uri
     * @return string
     */
    public function requestUri()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            $uri = trim($_SERVER['REQUEST_URI'], '/');
            // truncates GET params
            return (substr($uri, 0, 1) != '?') ? $uri : null;
        }
    }

    /**
     * Mathces current url with url in the app/config.php
     * @return string
     */
    private function matchRoutes()
    {
        $config = include APP_DIR . 'config.php';

        foreach ($config['url'] as $regex => $segment) {
            if (preg_match("~$regex~", $this->requestUri())) {
                $this->route = preg_replace("~$regex~", $segment, $this->requestUri());
            }
        } 
    }

    /**
     * Creates instance of \src\mod\Controller
     */
    public function direct()
    {
        $this->matchRoutes();

        $url = ($this->route) ? $this->route : '';

        $segments = explode('/', $url);
        $controllerName = ucfirst(array_shift($segments));
        $method = $this->prepareMethod(array_shift($segments));
        $params = (!empty($segments)) ? $segments : array();
        
        $controllerFile = APP_DIR . "controllers/{$controllerName}Controller.php";

        if (file_exists($controllerFile)) {
            include_once $controllerFile;
        } else {
            throw new \Exception("Error: file {$controllerFile} does not exist.");
        }
        
        $namespace = "\\app\controllers\\{$controllerName}Controller";
        $class = new $namespace;

        if (is_callable(array($class, "{$method}Action")) && $class instanceof Controller) {
            call_user_func_array(array($class, "{$method}Action"), $params);
        } else {
            throw new \Exception('Error: route ' . $this->requestUri() . ' not found.');
        }
    }

    /**
     * Generates proper method name
     * @param string $name
     * @return string
     */
    protected function prepareMethod($name)
    {
        if (explode('-', $name)) {
            $row = explode('-', $name);
            $tmp = [];

            $i = 0;
            while ($i < count($row)) {
                $tmp[$i] = ucfirst($row[$i]);
                $i++;
            }

            $properName = implode("", $tmp);

            return lcfirst($properName);
        } else {
            return $name;
        }
    }
}