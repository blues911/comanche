<?php

namespace src\mod;

use src\mod\Request;
use src\lib\Template;

class Controller
{
    /**
     * Instance of the Template class
     * @var object
     */  
    public $tpl;

    /**
     * Class constructor
     */   
    public function __construct()
    {
        $this->tpl = new Template;
    }

    public function redirect($path = null)
    {
        header("Location: " . url($path));
        exit;
    }
}