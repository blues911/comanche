<?php

class Terminal
{
    public function __construct($argv)
    {
        if (is_callable(array($this, $argv[1]))) {
            call_user_func(array($this, $argv[1])); 
        } else {
            exit("Error: method doesn't exist!\n" . PHP_EOL);
        }
    }

    public function help()
    {
        $info = "Comanche (c) 2016\n\n";
        $info .= "File commands:\n";
        $info .= "runserver     Run built-in web server.\n";
        $info .= "file          Create new file [controllers, models].\n";
        $info .= "routes        Display app routes.\n";
        
        fwrite(STDOUT, $info . PHP_EOL);
    }

    public function runserver()
    {
        $host = 'localhost';
        $port = 8080;
        $rootDir = getcwd();
        $date = date('Y-m-d H:i:s');

        $info = "Comanche development server started at {$date}\n";
        $info .= "Listening on http://{$host}:{$port}/\n";
        $info .= "Document root is {$rootDir}\n";
        $info .= "Press Ctrl+C to quit.\n";

        fwrite(STDOUT, $info . PHP_EOL);

        shell_exec('"'.PHP_BINARY.'"'." -S {$host}:{$port} -t {$rootDir}");
    }

    public function file()
    {   
        $type = ['controllers', 'models'];

        fwrite(STDOUT, "Set file namespace: ");
        $namespace = trim(fgets(STDIN));
        $row = explode('\\', $namespace);

        if (count($row) === 3 && in_array($row[1], $type)) {
            $this->createFile($row[1], $row[2], $namespace);
        } else {
            exit("Error: incorrect namespace!\n" . PHP_EOL);
        }
    }
    
    protected function createFile($type, $name, $namespace)
    {
        $path = "app/{$type}/{$name}.php";

        if (file_exists($path)) {
            exit("Error: {$path} is already exist!\n" . PHP_EOL);
        }
        
        $content = "<?php\n\nnamespace {$namespace};\n\nclass {$name}\n{}";

        $file = fopen($path, "w") or die("Error: unable to create file!\n" . PHP_EOL);
        fwrite($file, $content);
        fclose($file);
        fwrite(STDOUT, "Success: {$path} has been created.\n" . PHP_EOL);
    }

    public function routes()
    {
        $config = include __DIR__ . '/../../../app/config.php';
        
        fwrite(STDOUT, "app/config.php routes:" . PHP_EOL);
        fwrite(STDOUT, PHP_EOL);

        $i = 1;
        foreach ($config['url'] as $k => $v) {
            fwrite(STDOUT, "[{$i}] {$k}" . PHP_EOL);
            $i++;
        }
        
        fwrite(STDOUT, PHP_EOL);
    }    
}