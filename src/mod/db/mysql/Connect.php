<?php

namespace src\mod\db\mysql;

use src\Comanche;

class Connect
{
    /**
     * Instace of PDO class
     * @var object
     */    
    public $db;

    public function __construct()
    {
        $config = include APP_DIR . 'config.php';

        $dsn = $config['db']['dsn'];
        $database = $config['db']['database'];
        $hostname = $config['db']['hostname'];
        $charset = $config['db']['charset'];
        $username = $config['db']['username'];
        $password = $config['db']['password'];

        try {
            $this->db = new \PDO("$dsn:dbname=$database;host=$hostname;charset=$charset",
                $username, $password
            );
        } catch (\PDOException $error) {
            throw new \Exception("<br/> You have an error: " . $error->getMessage() . "<br/> On line: ".$error->getLine());
        }
    } 
}