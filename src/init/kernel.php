<?php
/**
 * Micro-kernel.
 * It's responsable for available "src" units and class autoload.
 */
class Kernel
{
    /**
     * Contains file path
     * @var string
     */    
    private static $file;

    /**
     * Contains sources path
     * @var array
     */        
    private static $map = [];

    /**
     * Contains system settings
     * @var array
     */     
    private static $config = [
        'php_version' => '5.3'
    ];

    /**
     * Runs system tests
     */
    public static function test()
    {
        // check php version
        if (!version_compare(phpversion(), self::$config['php_version'], '>')) {
            throw new \Exception("PHP version is not high enough!");
        }

        // check map units
        self::$map = include_once SRC_DIR . '/init/map.php';

        foreach (self::$map as $unit) {
            if (!file_exists($unit)) {
                throw new \Exception("Error: {$unit} not found.");
            }
        }        
    }

    /**
     * Loads class
     * @param string $class
     */
    public static function init($class)
    {
        self::$file = str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
        
        if (file_exists(self::$file)) {
            include_once self::$file;
        } else {
            throw new \Exception("Error: class {$class} does not exist.");
        }
    }
}

Kernel::test();
spl_autoload_register(['Kernel', 'init']);
