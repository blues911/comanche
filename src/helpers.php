<?php

/* global helpers */

function url($params = null)
{
    $protocol = 'http';

    if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off'
        || $_SERVER['SERVER_PORT'] == 443) {
        $protocol = 'https';
    }

    $domainName = $_SERVER['HTTP_HOST'];

    if ($params) {
        return "{$protocol}://{$domainName}{$params}";
    } else {
        return "{$protocol}://{$domainName}";
    }
}