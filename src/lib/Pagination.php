<?php

namespace src\lib;

use src\mod\URI;
use src\mod\Request;

class Pagination
{
    public $maxPosts;
    public $countPosts;
    public $openPage;
    public $path = null;

    private $page;    
    private $countPages;    

    private $uri;
    private $request;

    public function __construct($params)
    {
        $this->uri = new URI;
        $this->request = new Request;

        //$this->path = $this->uri->segment(0);
        $this->maxPosts = $params['maxPosts'];
        $this->countPosts = $params['countPosts'];
        
        $this->setValues();
    }
    
    private function setValues()
    {
        $this->countPages = ceil($this->countPosts / $this->maxPosts);
        $this->page = ($this->request->get('id')) ? $this->request->get('id') : 1;
        
        if ($this->page > $this->countPages) {
            $this->page = $this->countPages;
        }
            
        $this->openPage = ($this->page * $this->maxPosts) - $this->maxPosts;
    }
   
    public function setLinks()
    {
        $links = "<ul class='pagination'>";
        
        for ($i = 1; $i <= $this->countPages; $i++) {
            if ($i == $this->page) {
                $links .= "<li class='active'><a href='#''>{$i}</li>";
            } else {
                $links .= "<li><a href='".base_url("{$this->path}page?id={$i}")."'>{$i}</a></li>";
            }
        }
        
        $links .= "</ul>";

        return $links;
    }
}