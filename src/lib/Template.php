<?php
/**
 * Template for php views.
 * Supports render layout with views and single file.
 */
namespace src\lib;

class Template
{
    /**
     * Layout name
     * @var string
     */
    public $layout = null;

    /**
     * Container for view params
     * @var array
     */
    private $vars = [];

    /**
     * Renders view with layout
     * @param string $view
     * @param array $params
     */
    public function render($view, $params = null)
    {
        $layout = APP_DIR . "views/layouts/{$this->layout}.php";

        if (file_exists($layout)) {
            
            if ($params) {
                $this->vars = $this->setParams($params);
            }

            $this->vars['content'] = APP_DIR . "views/{$view}.php";
            extract($this->vars);
            include $layout;

            ob_start();
            $buffer = ob_get_contents();
            @ob_end_clean();

            echo $buffer;

        } else {
            throw new \Exception("Error: layout {$this->layout} does not exist");
        }
    }

    /**
     * Renders single file
     * @param string $file
     * @param array $params
     */
    public function renderFile($file, $params = null)
    {
        $view = APP_DIR . "views/{$file}.php";

        if (file_exists($view)) {

            if ($params) {
                $this->vars = $this->setParams($params);
                if ($this->vars) {
                    extract($this->vars);
                }
            }

            include $view;

            ob_start();
            $buffer = ob_get_contents();
            @ob_end_clean();
            
            echo $buffer;

        } else {
            throw new \Exception("Error: file {$view} does not exist");
        }
    }

    /**
     * Returns array of parameters
     * @param array $params
     */
    private static function setParams($params)
    {
        $row = [];
        
        if (!empty($params)) {
            foreach ($params as $k => $v) {
                $row[$k] = $v;
            }

            return $row;
        }

        return null;
    }  
}
